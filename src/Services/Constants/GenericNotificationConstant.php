<?php

namespace App\GenericNotification\Notification\Services\Constants;

interface GenericNotificationConstant extends MediumType, GenericNotificationType, StatusType
{

    const TYPES = [
        self::GENERAL => 'GENERAL',
        self::PROMOTION => 'PROMOTION',
        self::NEWS => 'NEWS',
        self::REMINDER => 'REMINDER',
    ];

    const STATUSES = [
        self::IN_PROCESS => 'IN PROCESS',
        self::IN_QUEUE => 'IN QUEUE',
        self::SENT => 'SENT',
        self::OPEN => 'OPEN',
        self::FAILED => 'FAILED',
    ];

    const MEDIUMS = [
        self::APP => 'APP',
        self::MAIL => 'MAIL',
        self::SMS => 'SMS',
    ];
}
