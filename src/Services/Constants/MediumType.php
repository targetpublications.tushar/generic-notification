<?php
namespace App\GenericNotification\Notification\Services\Constants;

interface MediumType
{
    const APP = 0;
    const MAIL = 1;
    const SMS = 2;
}
