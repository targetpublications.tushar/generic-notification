<?php
namespace App\GenericNotification\Notification\Services\Constants;

interface GenericNotificationType
{

    const GENERAL = 0;
    const PROMOTION = 1;
    const NEWS = 2;
    const REMINDER = 3;

}
