<?php

namespace App\GenericNotification\Notification\Services;

use App\GenericNotification\Notification\Models\GenericNotification;
use App\GenericNotification\Notification\Models\GenericNotificationLog;
use Illuminate\Http\Request;
use Carbon\Carbon;

class GenericNotificationLogService
{

    public Request $request;

    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    /**
     * getHttpMethod
     *
     * @return string
     */
    public function getHttpMethod(): string
    {
        return $this->request->method();
    }

    /**
     * getRequestUrl
     *
     * @return string
     */
    public function getRequestUrl(): string
    {
        return $this->request->url();
    }

    /**
     * getIpAddress
     *
     * @return string
     */
    public function getIpAddress(): string|null
    {
        return $this->request->ip();
    }

    /**
     * getUserAgent
     *
     * @return string
     */
    public function getUserAgent(): string|null
    {
        return $this->request->userAgent();
    }

    /**
     * gethostName
     *
     * @return string
     */
    public function gethostName(): string|null
    {
        return gethostbyaddr($this->getIpAddress());
    }

    /**
     * getOpenedAt
     *
     * @return Carbon
     */
    public function getOpenedAt(): Carbon
    {
        return Carbon::now();
    }

    /**
     * persist
     *
     * @param  mixed $identifier
     * @return GenericNotificationLog
     */
    public function persist(string $identifier): GenericNotificationLog
    {
        MarkMailRead::handle($identifier);
        $genericNotification = GenericNotification::where('identifier', $identifier)->firstOrFail();
        $genericNotificationLogData = [
            'generic_notification_id' => $genericNotification->id,
            'http_method' => $this->getHttpMethod(),
            'request_url' => $this->getRequestUrl(),
            'ip_address' => $this->getIpAddress(),
            'user_agent' => $this->getUserAgent(),
            'host_name' => $this->getHostName(),
            'opened_at' => $this->getOpenedAt()
        ];

        return GenericNotificationLog::persistCreateGenericNotificationLog($genericNotificationLogData);

    }

    public function getNotificationData(int $length, $start, int $draw, array $customSearch)
    {
        $totalData = GenericNotification::count();

        $limit = $length;
        $start = $start;

        $query = GenericNotification::query();
        if (isset($customSearch['type'])) {
            $query->where('type',  $customSearch['type']);
        }

        if (isset($customSearch['status'])) {
            $query->where('status',  $customSearch['status']);
        }

        if (isset($customSearch['medium'])) {
            $query->where('medium',  $customSearch['medium']);
        }

        if (isset($customSearch['search'])) {
            $query->where('data', 'LIKE', '%' . $customSearch['search'] . '%');
        }

        $totalFiltered = $query->count();

        $data = $query->offset($start)
            ->limit($limit)->orderBy('id', 'DESC')->get();

        return [
            'draw' => intval($draw),
            'recordsTotal' => intval($totalData),
            'recordsFiltered' => intval($totalFiltered),
            'data' => $data,
        ];
    }

    public function getNotificationLogsData(GenericNotification $genericNotification, int $length, $start, int $draw, array $customSearch)
    {
        $totalData = $genericNotification->genericNotificationLogs()->count();

        $limit = $length;
        $start = $start;

        $query = $genericNotification->genericNotificationLogs();

        $totalFiltered = $query->count();

        $data = $query->offset($start)
            ->limit($limit)->orderBy('id', 'DESC')->get();

        return [
            'draw' => intval($draw),
            'recordsTotal' => intval($totalData),
            'recordsFiltered' => intval($totalFiltered),
            'data' => $data,
        ];
    }
}
