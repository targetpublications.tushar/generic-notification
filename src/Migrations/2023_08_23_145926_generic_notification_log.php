<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('generic_notification_logs', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('generic_notification_id');
            $table->string('http_method');
            $table->string('request_url');
            $table->string('ip_address')->nullable();
            $table->string('user_agent')->nullable();
            $table->string('host_name');
            $table->timestamp('opened_at')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('generic_notification_logs');
    }
};
