<?php
namespace Tests\Unit\GenericNotification\Notification\Services;

use Tests\TestCase;
use App\GenericNotification\Notification\Services\GenericNotificationLogService;
use Illuminate\Http\Request;
use App\GenericNotification\Notification\Models\GenericNotification;
use App\GenericNotification\Notification\Models\GenericNotificationLog;
use Carbon\Carbon;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Mockery;

class GenericNotificationLogServiceTest extends TestCase
{
    use RefreshDatabase;

    public function testGetHttpMethodShouldReturnGet()
    {
        $request = new Request();
        $request->setMethod('GET');

        $service = new GenericNotificationLogService($request);

        $this->assertEquals('GET', $service->getHttpMethod());
    }

    public function testGetRequestUrlShouldReturnTestUrl()
    {
        $request = new Request();
        $request->server->set('REQUEST_URI', '/test-url');

        $service = new GenericNotificationLogService($request);

        $this->assertEquals('/test-url', $service->getRequestUrl());
    }

    public function testGetIpAddress()
    {
        $request = new Request();
        $request->server->set('REMOTE_ADDR', '127.0.0.1');

        $service = new GenericNotificationLogService($request);

        $this->assertEquals('127.0.0.1', $service->getIpAddress());
    }

    public function testGetUserAgentShouldReturnChrome()
    {
        $request = new Request();
        $request->headers->set('User-Agent', 'Chrome');
        $service = new GenericNotificationLogService($request);

        $this->assertEquals('Chrome', $service->getUserAgent());
    }

    public function testGetHostName()
    {
        $request = new Request();
        $request->server->set('REMOTE_ADDR', '127.0.0.1');
        $service = Mockery::mock(GenericNotificationLogService::class);
        $service->shouldReceive('gethostName')->andReturn('test-host-name');
        $this->assertEquals('test-host-name', $service->getHostName());
    }

    public function testGetOpenedAt()
    {
        $service = new GenericNotificationLogService(new Request());

        $this->assertInstanceOf(Carbon::class, $service->getOpenedAt());
    }

    public function testGenericNotificationLogPersist()
    {

        GenericNotification::create(
                    [
                        'identifier' => '232112312313',
                        'data' => ['key', 'value']
                    ]
                );
        $request = new Request();
        $request->setMethod('GET');
        $request->server->set('REQUEST_URI', '/test-url');
        $request->server->set('REMOTE_ADDR', '127.0.0.1');
        $request->headers->set('User-Agent', 'Chrome');

        $service = new GenericNotificationLogService($request);

        $log = $service->persist('232112312313');
        $this->assertInstanceOf(GenericNotificationLog::class, $log);
        $this->assertDatabaseCount('generic_notification_logs', 1);
        $this->assertEquals('GET', $log->http_method);
        $this->assertEquals('Chrome', $log->user_agent);
        $this->assertEquals('127.0.0.1', $log->ip_address);

    }
}
