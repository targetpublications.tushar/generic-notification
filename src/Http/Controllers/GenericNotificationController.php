<?php

namespace App\GenericNotification\Notification\Http\Controllers;

use App\GenericNotification\Notification\Models\GenericNotification;
use App\GenericNotification\Notification\Models\GenericNotificationLog;
use App\GenericNotification\Notification\Services\GenericNotificationLogService;
use App\GenericNotification\Notification\Services\MarkMailRead;
use App\Http\Controllers\Controller;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Illuminate\Http\Request;

class GenericNotificationController extends Controller
{
    /**
     * trackEmail
     *
     * @param  string $identifier
     * @throws ModelNotFoundException
     * @return BinaryFileResponse
     */
    public function trackEmail(Request $request, string $identifier): BinaryFileResponse
    {

        $genericNotificationLogService = new GenericNotificationLogService($request);
        // if (strpos($genericNotificationLogService->getUserAgent(), 'ggpht.com GoogleImageProxy') == false) {
        //     $genericNotificationLogService->persist($identifier);
        // }
        $genericNotificationLogService->persist($identifier);
        //Return a transparent pixel image in the email.
        return response()->file(public_path('generic-notification/pixel.png'), [
            'Content-Type' => 'image/png',
        ]);
    }

    public function genericNotifications()
    {
        return view('App\GenericNotification\Notification::dashboard');
    }

    public function genericNotificationsAjax(Request $request)
    {
        $length = $request->input('length');
        $start = $request->input('start');
        $draw = $request->input('draw');
        $customSearch['type'] = $request->type;
        $customSearch['status'] = $request->status;
        $customSearch['medium'] = $request->medium;
        $customSearch['search'] = $request->search;

        return response()->json((new GenericNotificationLogService($request))->getNotificationData($length, $start, $draw, $customSearch));
    }



    public function genericNotificationLogs(GenericNotification $genericNotification)
    {
        return view('App\GenericNotification\Notification::show', compact('genericNotification'));
    }

    public function genericNotificationLogsAjax(Request $request, GenericNotification $genericNotification)
    {
        $length = $request->input('length');
        $start = $request->input('start');
        $draw = $request->input('draw');
        $customSearch['type'] = $request->type;
        $customSearch['status'] = $request->status;
        $customSearch['medium'] = $request->medium;
        $customSearch['search'] = $request->search;
        return response()->json((new GenericNotificationLogService($request))->getNotificationLogsData($genericNotification, $length, $start, $draw, $customSearch));
    }

}
