@extends('App\GenericNotification\Notification::_master')
@section('content')
    <div class="page-header mb-4">
        <h1>@lang('Generic Notification Log')</h1>
    </div>

    <div class="container-fluid">
        <div class="row">
            <div class="col-md-2">
                <label>Type</label>
                <input type="text" class="form-control"
                    value="{{ App\GenericNotification\Notification\Services\Constants\GenericNotificationConstant::TYPES[$genericNotification->type] }}"
                    readonly />
            </div>
            <div class="col-md-2">
                <label>Medium</label>
                <input type="text" class="form-control"
                    value="{{ App\GenericNotification\Notification\Services\Constants\GenericNotificationConstant::MEDIUMS[$genericNotification->medium] }}"
                    readonly />
            </div>
            <div class="col-md-2">
                <label>Status</label>
                <input type="text" class="form-control"
                    value="{{ App\GenericNotification\Notification\Services\Constants\GenericNotificationConstant::STATUSES[$genericNotification->status] }}"
                    readonly />
            </div>
            <div class="col-md-2">
                <label>Reciver</label>
                <input type="text" class="form-control"
                    value="{{ App\GenericNotification\Notification\Services\Constants\GenericNotificationConstant::STATUSES[$genericNotification->status] }}"
                    readonly />
            </div>
        </div>
        <div class="row" style="
        padding-top: 10px;
        padding-bottom: 20px;
    ">
            <div class="col-md-5">
                <label>Data</label>
                @if (!empty($genericNotification->data['message']))
                    <div class="message_data" style="max-height: 500px; overflow-y: auto;">
                        <pre>{{ json_encode($genericNotification->data, JSON_PRETTY_PRINT) }}</pre>
                    </div>
                @endif
            </div>
            <div class="col-md-5 content-container">
                <label>Message View</label>
                <div class="html_data" style="max-height: 500px; overflow-y: auto;">
                    @if (
                        $genericNotification->medium ==
                            App\GenericNotification\Notification\Services\Constants\GenericNotificationConstant::MAIL)
                    @elseif (
                        $genericNotification->medium ==
                            App\GenericNotification\Notification\Services\Constants\GenericNotificationConstant::SMS)
                        {!! urldecode($genericNotification->data['message']) !!}
                    @else
                        {!! urldecode($genericNotification->data['message']) !!}
                    @endif
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="row">
                    <div class="col-md-12">
                        <table id="data-table" class="display nowrap" style="width:100%">
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Http Method</th>
                                    <th>Open At</th>
                                    <th>Request URL</th>
                                    <th>User Agent</th>
                                    <th>Host Name</th>
                                    <th>Created At</th>
                                    <th>Updated At</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script>
        var types = @json(App\GenericNotification\Notification\Services\Constants\GenericNotificationConstant::TYPES);

        var mediums = @json(App\GenericNotification\Notification\Services\Constants\GenericNotificationConstant::MEDIUMS);

        var statuses = @json(App\GenericNotification\Notification\Services\Constants\GenericNotificationConstant::STATUSES);

        jQuery(document).ready(function($) {
            var table = $('#data-table').DataTable({
                processing: true,
                serverSide: true,
                responsive: true, // Enable responsive feature
                ajax: {
                    url: "{{ route('generic-notifications-logs.ajax', $genericNotification->id) }}",
                    type: 'get',
                    data: function(d) {
                        $('.custom_search_field').each(function() {
                            d[$(this).attr('name')] = $(this).val();
                        });

                    }
                },
                columns: [{
                        data: 'id',
                        name: 'id'
                    },
                    {
                        data: 'http_method',
                        name: 'http_method',
                    },
                    {
                        data: 'opened_at',
                        name: 'opened_at'
                    },
                    {
                        data: 'request_url',
                        name: 'request_url',
                        render: function(data, type, row) {
                            return '<a href=' + row.request_url +
                                ' class="btn btn-show" data-id="' + row.id +
                                '">Reuest Url</a>';
                        }
                    },
                    {
                        data: 'user_agent',
                        name: 'user_agent',
                        render: function(data, type, row) {
                            return row.user_agent;
                        }
                    },
                    {
                        data: 'host_name',
                        name: 'host_name'
                    },

                    {
                        data: 'created_at',
                        name: 'created_at'
                    },
                    {
                        data: 'updated_at',
                        name: 'updated_at'
                    }
                ],
                searching: false
            });

        });
    </script>
@endsection
