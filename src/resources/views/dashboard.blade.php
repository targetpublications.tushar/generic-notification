@extends('App\GenericNotification\Notification::_master')

@section('content')
    <div class="page-header mb-4">
        <h1>@lang('Dashboard')</h1>
    </div>

    <div class="container-fluid">
        <div class="row ">
            <div class="col-md-2">
                <label>Type</label>
                <select name="type" id="type" class="form-control custom_search_field">
                    <option value="">Select Type</option>
                    @foreach (App\GenericNotification\Notification\Services\Constants\GenericNotificationConstant::TYPES as $key => $type)
                        <option value="{{ $key }}">{{ $type }}</option>
                    @endforeach
                </select>
            </div>
            <div class="col-md-2">
                <label>Medium</label>
                <select name="medium" id="medium" class="form-control custom_search_field">
                    <option value="">Select Medium</option>
                    @foreach (App\GenericNotification\Notification\Services\Constants\GenericNotificationConstant::MEDIUMS as $key => $medium)
                        <option value="{{ $key }}">{{ $medium }}</option>
                    @endforeach
                </select>
            </div>
            <div class="col-md-2">
                <label>Status</label>
                <select name="status" id="status" class="form-control custom_search_field">
                    <option value="">Select Status</option>
                    @foreach (App\GenericNotification\Notification\Services\Constants\GenericNotificationConstant::STATUSES as $key => $status)
                        <option value="{{ $key }}">{{ $status }}</option>
                    @endforeach
                </select>
            </div>
            <div class="col-md-2">
                <label>Search</label>
                <input type="text" name="search" id="search" class="form-control custom_search_field" />
                </select>
            </div>
            <div class="col-md-2" style="margin-top:30px; ">
                <button class="btn btn-primary custom_search">Search</button> <button class="btn btn-defualt custom_clear_search">Clear</button>
            </div>

            <div class="col-md-12" style="margin-top:20px">
                <table id="data-table" class="display nowrap" style="width:100%">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Type</th>
                            <th>Mediam</th>
                            <th>Sent At</th>
                            <th>Status</th>
                            <th>Open At</th>
                            <th>Open Count</th>
                            <th>Created At</th>
                            <th>Updated At</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script>
        var types = @json(App\GenericNotification\Notification\Services\Constants\GenericNotificationConstant::TYPES);

        var mediums = @json(App\GenericNotification\Notification\Services\Constants\GenericNotificationConstant::MEDIUMS);

        var statuses = @json(App\GenericNotification\Notification\Services\Constants\GenericNotificationConstant::STATUSES);

        jQuery(document).ready(function($) {
            var showRoute = "{{ route('generic-notification-logs', '_ID') }}";
            var table = $('#data-table').DataTable({
                processing: true,
                serverSide: true,
                responsive: true, // Enable responsive feature
                ajax: {
                    url: "{{ route('generic-notifications.ajax') }}",
                    type: 'get',
                    data: function(d) {
                        $('.custom_search_field').each(function() {
                            d[$(this).attr('name')] = $(this).val();
                        });

                    }
                },
                columns: [{
                        data: 'id',
                        name: 'id'
                    },
                    {
                        data: 'type',
                        name: 'type',
                        render: function(data, type, row) {
                            return types[row.type];
                        }
                    },
                    {
                        data: 'medium',
                        name: 'medium',
                        render: function(data, type, row) {
                            return mediums[row.medium];
                        }
                    },
                    {
                        data: 'sent_at',
                        name: 'sent_at'
                    },
                    {
                        data: 'status',
                        name: 'status',
                        render: function(data, type, row) {
                            return statuses[row.status];
                        }
                    },
                    {
                        data: 'opened_at',
                        name: 'opened_at'
                    },
                    {
                        data: 'open_count',
                        name: 'open_count'
                    },
                    {
                        data: 'created_at',
                        name: 'created_at'
                    },
                    {
                        data: 'updated_at',
                        name: 'updated_at'
                    },
                    {
                        data: null,
                        name: 'Action',
                        orderable: false,
                        render: function(data, type, row) {

                            return '<a href='+showRoute.replace('_ID', row.id)+' class="btn btn-show" data-id="' + row.id +
                                '">View</a>';
                        }
                    },
                ],
                searching: false
            });
            $('.custom_search').on('click', function() {
                var searchTerms = []; // Initialize an array to hold search terms

                $('.custom_search_field').each(function() {
                    var value = $(this).val();
                    if (value) {
                        searchTerms.push(value);
                    }
                });

                var searchString = searchTerms.join(' ');
                table.search(searchString).draw();
            });

            $('.custom_clear_search').on('click', function() {
                var searchTerms = []; // Initialize an array to hold search terms

                $('.custom_search_field').each(function() {
                    var value = $(this).val("");
                });

                var searchString = searchTerms.join(' ');
                table.search(searchString).draw();
            });
        });
    </script>
@endsection
