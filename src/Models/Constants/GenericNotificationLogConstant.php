<?php
namespace App\GenericNotification\Notification\Models\Constants;

interface GenericNotificationLogConstant
{
    public const CREATE_RULE = [
        "http_method"=>'required',
        "request_url" => 'required',
        'ip_address'=>'required',
        'user_agent'=>'required',
        'host_name'=>'required',
        'opened_at'=>'required',
    ];
}
