<?php

namespace App\GenericNotification\Notification\Models;

use App\GenericNotification\Notification\Models\Constants\GenericNotificationLogConstant;
use App\Helpers\Services\Utils;
use Illuminate\Database\Eloquent\Model;

class GenericNotificationLog extends Model implements GenericNotificationLogConstant
{

    protected $fillable = ['generic_notification_id', 'http_method', 'request_url', 'ip_address', 'user_agent', 'host_name', 'opened_at'];

    /**
     * getCreateValidationRules
     *
     * @return array<string,string>
     */
    public static function getCreateValidationRules(): array
    {
        return self::CREATE_RULE;
    }

    /**
     * persistCreateGenericNotificationLog
     *
     * @param  array<string,mixed> $GenericNotificationLogData
     * @return GenericNotificationLog
     */
    public static function persistCreateGenericNotificationLog(array $GenericNotificationLogData): GenericNotificationLog
    {
        Utils::validateOrThrow(self::getCreateValidationRules(), $GenericNotificationLogData);
        return GenericNotificationLog::create($GenericNotificationLogData);
    }
}
