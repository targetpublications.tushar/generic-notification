<?php

use App\GenericNotification\Notification\Http\Controllers\GenericNotificationController;
use Illuminate\Support\Facades\Route;

Route::get('/track/email/{unique_identifier}/pixel.png', [GenericNotificationController::class, 'trackEmail'])->name("track.email")->withoutMiddleware('auth');
Route::get('/generic-notifications', [GenericNotificationController::class, 'genericNotifications'])->name("generic-notifications");
Route::get('/generic-notifications/ajax', [GenericNotificationController::class, 'genericNotificationsAjax'])->name("generic-notifications.ajax");
Route::get('/generic-notifications/{genericNotification}/logs/', [GenericNotificationController::class, 'genericNotificationLogs'])->name("generic-notification-logs");
Route::get('/generic-notifications/{genericNotification}/logs/ajax', [GenericNotificationController::class, 'genericNotificationLogsAjax'])->name("generic-notifications-logs.ajax");
